Feature: Get current weather data
Background:

  Scenario Outline: Get Current weather in a location
    When I query weather for a location <Location> with units set to <Unit> and access_key 1d17e53d1572431cf5d2f8b51fc8e2c7
    Then the status code should be 200
    Then the result should be for <Location> and current temperature is present
    Then the result type should contain <city>
    Then the result query should contain <query>
    Then the result language should contain <language>
    Then the result unit should contain <Unit>
    Then the result country should contain <country>
    Then the result region should contain <region>
    Then the result latitude should contain <latitude>
    Then the result longitude should contain <longitude>
    Then the result timezone_id should contain <timezone_id>
    Examples:
      | Location     | Unit |city |query                  |language  |country     |region        |latitude |longitude |timezone_id      |
      | Amsterdam    | m    |City |Amsterdam, Netherlands |en        |Netherlands |North Holland |52.374   |4.890     |Europe/Amsterdam |
      | Barcelona    | s    |City |Barcelona, Spain       |en        |Spain       |Catalonia     |41.383   |2.183     |Europe/Madrid    |

  Scenario Outline: Get Current weather using invalid unit
    When I query weather for a location <Location> with units set to <Unit> and access_key 1d17e53d1572431cf5d2f8b51fc8e2c7
    Then the status code should be 200
    Then the result is an error with type <ErrorType>
    Then the result is an error code 606
    Then the result is an error message <info>
    Examples:
      | Location     | Unit |ErrorType    |info                                                                                    |
      | Amsterdam    | z    |invalid_unit |You have specified an invalid unit. Please try again or refer to our API documentation. |

  Scenario Outline: Get Current weather using invalid or empty Location and empty Unit
    When I query weather for a location <Location> with units set to <Unit> and access_key 1d17e53d1572431cf5d2f8b51fc8e2c7
    Then the status code should be 200
    Then the result is an error with type <ErrorType>
    Then the result is an error code <error>
    Then the result is an error message <info>
    Examples:
      | Location  | Unit |ErrorType      |info                                                                  |error |
      | x         | m    |request_failed |Your API request failed. Please try again or contact support.         |615   |
      |           |      |missing_query  |Please specify a valid location identifier using the query parameter. |601   |

  Scenario Outline: Get Current weather using an empty Location Access Key
    When I query weather for a location <Location> with units set to <Unit> and access_key <key>
    Then the status code should be 200
    Then the result is an error with type <ErrorType>
    Then the result is an error code <error>
    Then the result is an error message <info>
    Examples:
      |key | Location  | Unit |ErrorType          |info                                                                                    |error |
      |    | Amsterdam | m    |missing_access_key |You have not supplied an API Access Key. [Required format: access_key=YOUR_ACCESS_KEY]  |101   |
      |w   | Amsterdam | m    |invalid_access_key |You have not supplied a valid API Access Key. [Technical Support: support@apilayer.com] |101   |