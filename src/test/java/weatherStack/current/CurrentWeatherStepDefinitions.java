package weatherStack.current;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

import java.util.TimeZone;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.equalTo;

public class CurrentWeatherStepDefinitions {

    @Steps
    CurrentWeatherApi currentWeatherApi;

    @When("I query weather for a location {} with units set to {} and access_key {}")
    public void queryCurrentWeather(String location, String unit, String access_key) {
        currentWeatherApi.queryCurrentWeatherByLocationAndUnit(location, unit, access_key);
    }
    @Then("the status code should be {int}")
    public void the_status_code_should_be(Integer int1){
    restAssuredThat(response -> response.statusCode(200));
    }
    @Then("the result should be for {} and current temperature is present")
    public void theResultingLocationShouldBe(String location) {
        restAssuredThat(response -> response.body("'location'.'name'", equalTo(location)));
    }
    @Then("the result is an error with type {}")
    public void theResultWithErrorInvalidUnit(String errorType) {
        restAssuredThat(response -> response.body("'error'.'type'", equalTo(errorType)));
    }
    @Then("the result type should contain {}")
    public void requestCity(String city) {
        restAssuredThat(response -> response.body("'request'.'type'", equalTo(city)));
    }
    @Then("the result query should contain {}")
    public void requestQuery(String query) {
        restAssuredThat(response -> response.body("'request'.'query'", equalTo(query)));
    }
    @Then("the result language should contain {}")
    public void requestLanguage(String language) {
        restAssuredThat(response -> response.body("'request'.'language'", equalTo(language)));
    }
    @Then("the result unit should contain {}")
    public void requestunit(String unit) {
        restAssuredThat(response -> response.body("'request'.'unit'", equalTo(unit)));
    }
    @Then("the result country should contain {}")
    public void requestCountry(String country) {
        restAssuredThat(response -> response.body("'location'.'country'", equalTo(country)));
    }
    @Then("the result region should contain {}")
    public void requestRegion(String region) {
        restAssuredThat(response -> response.body("'location'.'region'", equalTo(region)));
    }
    @Then("the result latitude should contain {}")
    public void requestLatitude(String latitude) {
        restAssuredThat(response -> response.body("'location'.'lat'", equalTo(latitude)));
    }
    @Then("the result longitude should contain {}")
    public void requestLongitude(String longitude) {
        restAssuredThat(response -> response.body("'location'.'lon'", equalTo(longitude)));
    }
    @Then("the result timezone_id should contain {}")
    public void requestTimezone_id(String timezone_id) {
        restAssuredThat(response -> response.body("'location'.'timezone_id'", equalTo(timezone_id)));
    }
    @Then("the result is an error code {}")
    public void errorCode(Integer errorCode) {
        restAssuredThat(response -> response.body("'error'.'code'", equalTo(errorCode)));
    }
    @Then("the result is an error message {}")
    public void errorMessage(String info) {
        restAssuredThat(response -> response.body("'error'.'info'", equalTo(info)));
    }
}
