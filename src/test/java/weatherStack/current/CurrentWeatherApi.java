package weatherStack.current;

import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class CurrentWeatherApi {

    private static String CURRENT_BY_LOCATION_AND_UNIT = "http://api.weatherstack.com/current?access_key={access_key}&query={location}&units={unit}";

    @Step("query weather for a location {0} with units set to {1} and access Key {2}")
    public void queryCurrentWeatherByLocationAndUnit(String location, String unit, String access_key) {
        SerenityRest.given()
                .pathParam("location", location)
                .pathParam("unit", unit)
                .pathParam("access_key",access_key)
                .get(CURRENT_BY_LOCATION_AND_UNIT);
    }
}
