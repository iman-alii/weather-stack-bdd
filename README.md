# Weather Stack API test 
- In this project I covered some of the Negative and positive test cases for the Weather API https://weatherstack.com/
- Sample request --> GET: http://api.weatherstack.com/current?access_key=ecc7d88d942f0c2d7c69b67c70effe9e&query=Amsterdam&unit=m 
- The response contains weather information about the input city 

## Frameworks & Tools
- Cucumber 6 
- Serenity
- Intellij IDEA
- GitLab & GitLab Actions
- Maven

## Scope
- The test was done only for the Current API, the Historical API, forecaset are not available on the free plan

## Test Coverage 
Scenarios covered: 
- Positive scenario with proper input city and unit - Assertions were made on the response body and the response status code.
- Negative scenario using invalid unit - Assertions were made on the response status code, error code and error message. 
- Negative scenario using invalid or empty location - Assertions were made on the response status code, error type and information message. 
- Negative scenario using invalid or empty access key - Assertions were made on the response status code, Error Type and information message. 

The assertions were made on some response samples, I did not cover everything in the response as it will just require some repetitive work :) 

## Approach & Considerations
- Used maven to manage dependencies and build
- continuous Integration using GitLab workflows
- All test cases should pass

## How to run the test
- Go to GitLab Menu --> project: weather-stack-bdd --> from left side menu select CICD --> Pipelines --> Run pipeline --> the test will be launched automatically 
- To run the project on local machine, this can be done using the Maven command : mvn clean verify
- The test will Automatically trigger when new Commit or a pull request is merged to the main branch
